public class Fourgonnette extends Utilitaire {
    public Fourgonnette(String immatriculation, int nbChevaux, double consLitres100Km, double hauteur) {
        super(immatriculation, nbChevaux, consLitres100Km, hauteur);
    }

    @Override
    public String toString() {
        return "Une fourgonnette " + super.toString();
    }
}
