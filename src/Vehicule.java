import java.util.Objects;

/**
 * Classe abstraite, car il ne fait pas sens de déclarer un object véhicule qui ne soit pas une instance concrète.
 */
public abstract class Vehicule {
    private final String immatriculation;
    private final int nbChevaux;
    private final double consLitres100Km;

    public Vehicule(String immatriculation, int nbChevaux, double consLitres100Km) {
        if (consLitres100Km < 2) {
            throw new IllegalArgumentException("La consommation doit être supérieure à 2.0L/100km");
        }
        this.immatriculation = immatriculation;
        this.nbChevaux = nbChevaux;
        this.consLitres100Km = consLitres100Km;
    }

    public String getImmatriculation() {
        return immatriculation;
    }

    public int getNbChevaux() {
        return nbChevaux;
    }

    public double getConsLitres100Km() {
        return consLitres100Km;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Vehicule)) return false;
        return Objects.equals(immatriculation, ((Vehicule) o).getImmatriculation());
    }

    @Override
    public String toString() {
        return " d'immatriculation : " + immatriculation + ", de nombre de chevaux : " + nbChevaux
                + ", de consommation en litres pour 100 km : " + consLitres100Km;
    }

    @Override
    public int hashCode() {
        return immatriculation.hashCode();
    }

    public abstract double calculerCout();
}
