import java.util.ArrayList;

public class Test {
    public static void main(String[] args) {
        ArrayList<Vehicule> vehicules = new ArrayList<>();

        Vehicule voiture = new Voiture("0", 2, 4);
        Vehicule fourgonnette = new Fourgonnette("1", 10, 8, 3);
        Vehicule camion = new Camion("3", 20, 30, 4, 2500);

        vehicules.add(voiture);
        vehicules.add(fourgonnette);
        vehicules.add(camion);

        for (Vehicule vehicule : vehicules) {
            System.out.println(vehicule.toString());
            System.out.println("Cout : " + vehicule.calculerCout());
        }

        Vehicule voiture1 = new Voiture("0", 0, 2);
        System.out.println(voiture.equals(voiture1));

        System.out.println("Test du système de location");
        Loueur loueur = new Loueur();
        loueur.ajouterClient("Roussel", "Guillaume");
        loueur.ajouterClient("Chabanas", "Matthieu");
        System.out.println("Etat du loueur : " + loueur);

        loueur.ajouterVehicule(camion);
        loueur.ajouterVehicule(voiture);
        loueur.ajouterVehicule(fourgonnette);
        System.out.println("Etat du loueur : " + loueur);

        System.out.println("Le véhicule le plus rentable est : " + loueur.getMeilleurProfit());

        loueur.louer(camion, "Chabanas", "Matthieu");
        loueur.louer(voiture, "Roussel", "Guillaume");
        System.out.println("Etat du loueur : " + loueur);


        // Erreur attendue car le camion n'est plus libre
        try {
            loueur.louer(camion, "Roussel", "Guillaume");
        } catch (Exception e) {
            e.printStackTrace();
        }

        loueur.rendre(voiture);
        System.out.println("Etat du loueur : " + loueur);

    }
}
