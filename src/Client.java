import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Client {
    private final String nom;
    private final String prenom;

    /**
     * Véhicules loués
     */
    private final Set<Vehicule> vLoues = new HashSet<>();

    public Client(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    public void ajouterLocation(Vehicule vehicule) {
        vLoues.add(vehicule);
    }

    public void supprimerLocation(Vehicule vehicule) {
        vLoues.remove(vehicule);
    }

    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Client)) return false;
        return Objects.equals(nom, ((Client) o).getNom()) && Objects.equals(prenom, ((Client) o).getPrenom());
    }

    @Override
    public int hashCode() {
        return (nom + prenom).hashCode();
    }

    @Override
    public String toString() {
        return nom + " " + prenom + " : " + vLoues;
    }
}
