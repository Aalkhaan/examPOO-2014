import java.util.*;

public class Loueur {
    private final Collection<Vehicule> vLibres;
    private final Map<String, Client> clients;

    /**
     * Associe chaque véhicule loué au client concerné afin de retrouver le client instantanément
     * en cas de rente du véhicule.
     * <p> Lorsque qu'une méthode supprime un véhicule de {@code vLoues}, elle l'ajoute toujours à {@code vLibres}
     * et inversement.
     * Ainsi la contenance des collections {@code vLoues} et {@code vLibres} est toujours cohérente entre elles.
     */
    private final Map<Vehicule, Client> vLoues;

    private final TreeSet<Vehicule> vLibresGain;

    public Loueur() {
        vLibres = new HashSet<>();
        clients = new HashMap<>();
        vLoues = new HashMap<>();
        vLibresGain = new TreeSet<>(Comparator.comparing(Vehicule::calculerCout));
    }

    /**
     * Ajoute un nouveau véhicule libre.
     */
    public void ajouterVehicule(Vehicule vehicule) {
        vLibres.add(vehicule);
        vLibresGain.add(vehicule);
    }

    public Client ajouterClient(String nom, String prenom) {
        if (clients.containsKey(nom + prenom)) return clients.get(nom + prenom);

        Client nouveauClient = new Client(nom, prenom);
        clients.put(nom + prenom, nouveauClient);
        return nouveauClient;
    }

    /**
     * Loue un véhicule en complexité constante
     */
    public void louer(Vehicule vehicule, String nom, String prenom) {
        if (!vLibres.contains(vehicule)) throw new IllegalArgumentException("Le véhicule n'est pas libre");
        if (!clients.containsKey(nom + prenom)) throw new IllegalArgumentException("Le client n'existe pas");
        clients.get(nom + prenom).ajouterLocation(vehicule);
        vLibres.remove(vehicule);
        vLibresGain.remove(vehicule);
        vLoues.put(vehicule, clients.get(nom + prenom));
    }

    /**
     * Rend un véhicule loué disponible en complexité constante
     */
    public void rendre(Vehicule vehicule) {
        if (!vLoues.containsKey(vehicule)) throw new IllegalArgumentException("Ce véhicule n'est pas loué");
        vLoues.get(vehicule).supprimerLocation(vehicule);
        vLoues.remove(vehicule);
        vLibres.add(vehicule);
        vLibresGain.add(vehicule);
    }

    @Override
    public String toString() {
        StringBuilder toReturn = new StringBuilder("Véhicules libres : " + vLibres + "\n");
        for (Client client : clients.values()) {
            toReturn.append(client);
            toReturn.append("\n");
        }
        return toReturn.toString();
    }

    public Vehicule getMeilleurProfit() {
        return vLibresGain.last();
    }
}
