public class Voiture extends Vehicule {
    public Voiture(String immatriculation, int nbChevaux, double consLitres100Km) {
        super(immatriculation, nbChevaux, consLitres100Km);
    }

    @Override
    public String toString() {
        return "Une voiture" + super.toString();
    }

    @Override
    public double calculerCout() {
        return 20 + 2 * getNbChevaux();
    }
}
