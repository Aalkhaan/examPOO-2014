public class Camion extends Utilitaire {
    /**
     * Poids en kg
     */
    private final double poids;

    public Camion(String immatriculation, int nbChevaux, double consLitres100Km, double hauteur, double poids) {
        super(immatriculation, nbChevaux, consLitres100Km, hauteur);
        this.poids = poids;
    }

    @Override
    public String toString() {
        return "Un camion " + super.toString() + ", de poids : " + poids + " kg";
    }

    @Override
    public double calculerCout() {
        if (poids > 2199) {
            //noinspection IntegerDivisionInFloatingPointContext
            return ((int) poids - 2000) / 200 * 2 + super.calculerCout();
        }
        return super.calculerCout();
    }
}
