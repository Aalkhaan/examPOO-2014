public abstract class Utilitaire extends Vehicule {
    private final double hauteur;

    public Utilitaire(String immatriculation, int nbChevaux, double consLitres100Km, double hauteur) {
        super(immatriculation, nbChevaux, consLitres100Km);
        this.hauteur = hauteur;
    }

    @Override
    public String toString() {
        return super.toString() + ", de hauteur : " + hauteur + " m";
    }

    @Override
    public double calculerCout() {
        return 30 + 2 * getNbChevaux();
    }
}
